package tw.edu.nctu.ggg.mag_sensor;



import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.util.Log;



public class GPSTracker extends Service implements LocationListener  {

	private final Context mContext;

    // flag for GPS status 
    boolean isGPSEnabled = false;

    // flag for network status 
    boolean isNetworkEnabled = false;

    // flag for GPS status 
    boolean canGetLocation = false;
    public String provider;

    public Location location; // location 
    public double a=1;// latitude
    public double b=2;// longitude
    public double speed=0.0;//
    public double bearing=0.0;//degrees
    public double altitudeG=0.0,altitudeN=0.0;// altitude
    public double accuracy;
    public double vaccuracy;
    public double satellite_inview, satellite_inuse;
    public double pdop,hdop,vdop,ptype;

    public double getSatellite_inview() {
        return satellite_inview;
    }
    public double getSatellite_inuse() {
        return satellite_inuse;
    }
    public double getPdop() {
        return pdop;
    }

    public double getHdop() {
        return hdop;
    }

    public double getVdop() {
        return vdop;
    }
    public double getPtype() {
        return ptype;
    }

    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 0; // any change

    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 10 ; // 0.1 sec

    // Declaring a Location Manager
    public LocationManager locationManager;
    private long updateTime;

    public GPSTracker(Context context) {
        this.mContext = context;

        getLocation();
    }



    public Location getLocation() {
        try {
            locationManager = (LocationManager) mContext
                    .getSystemService(LOCATION_SERVICE);
            //add nmea listener 2018/06/18
            //locationManager.addNmeaListener(nmeaListener);

            // getting GPS status
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                // no network provider is enabled
            } else {
                this.canGetLocation = true;
                // First get location from Network Provider
                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    Log.d("Network", "Network");
                    this.provider="Network";
                    if (locationManager != null) {
                        //location = locationManager
                        //        .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            this.a = location.getLatitude();
                            this.b = location.getLongitude();
                        }
                    }
                }
                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    if (location == null) {
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                        Log.d("GPS Enabled", "GPS Enabled");
                        this.provider="GPS";
                        if (locationManager != null) {
                            //location = locationManager
                            //        .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location != null) {
                                this.a = location.getLatitude();
                                this.b = location.getLongitude();
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return location;
    }

    /**
     * Stop using GPS listener
     * Calling this function will stop using GPS in your app
     * */
    public void stopUsingGPS(){
        if(locationManager != null){
            locationManager.removeUpdates(GPSTracker.this);
            //locationManager.removeNmeaListener(nmeaListener);
        }
    }

    /**
     * Function to get latitude
     * */
    public double getLatitude(){

        // return latitude
        return a;
    }

    /**
     * Function to get longitude
     * */
    public double getLongitude(){

        // return longitude
        return b;
    }
    public double getSpeed(){
        return speed;
    }
    public double getBearing(){
        return bearing;
    }
    /**
     * Function to get altitude
     * */
    public double getAltitudeG(){
        return altitudeG;
    }
    public double getAltitudeN(){
        return altitudeN;
    }

    public Long getUpdateTime(){

        // return longitude
        return updateTime;
    }

    public double getAccuracy() {
        return accuracy;
    }
    public double getVaccuracy() {
        return vaccuracy;
    }


    /**
     * Function to check GPS/wifi enabled
     * @return boolean
     * */
    public boolean canGetLocation() {
        return this.canGetLocation;
    }

    /**
     * Function to show settings alert dialog
     * On pressing Settings button will lauch Settings Options
     * */
    public void showSettingsAlert(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);

        // Setting Dialog Title
        alertDialog.setTitle("GPS is settings");

        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                mContext.startActivity(intent);
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    public boolean isOpenGps() {
        // get the location by GPS
        boolean gps = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        // get the location by WLAN or mobile network. It's usually used at the place which is more hidden.
        boolean network = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (gps || network)
            return true;
        return false;
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
        if(location.getProvider().equals(LocationManager.GPS_PROVIDER)){
            this.altitudeG=location.getAltitude();
        }else if(location.getProvider().equals(LocationManager.NETWORK_PROVIDER)){
            this.altitudeN=location.getAltitude();
        }
		this.a=location.getLatitude();
		this.b=location.getLongitude();
        this.speed=location.getSpeed();
        this.bearing=location.getBearing();
        this.updateTime=System.currentTimeMillis();
        this.accuracy=location.getAccuracy();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            this.vaccuracy = location.getVerticalAccuracyMeters();
        }
		//Log.d("TAG", System.currentTimeMillis()+"New update!!"+"latitude: " +location.getLatitude()+"longitude: " +location.getLongitude());
		//Toast.makeText(getApplicationContext(), "New Location is - \nLat: " + this.a + "\nLong: " + this.b, Toast.LENGTH_LONG).show();
	}

	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

    private void nmeaProgress(String rawNmea){

        String[] rawNmeaSplit = rawNmea.split(",");
        try{
        if (rawNmeaSplit[0].equalsIgnoreCase("$GPGGA")){
            Log.d("dddGPS-NMEA", rawNmea);
            Log.d("dddGPS-NMEA", "7="+rawNmeaSplit[7]);
            Log.d("dddGPS-NMEA", "8="+ rawNmeaSplit[8]);
            if(!rawNmeaSplit[7].isEmpty())this.satellite_inuse=Double.parseDouble(rawNmeaSplit[7]);
            if(!rawNmeaSplit[8].isEmpty())this.hdop=Double.parseDouble(rawNmeaSplit[8]);

        }else if(rawNmeaSplit[0].equalsIgnoreCase("$GPGSA")){
            Log.w("dddGPS-NMEA", rawNmea);
            Log.w("dddGPS-NMEA", "num="+rawNmeaSplit.length);
            Log.w("dddGPS-NMEA", "15="+rawNmeaSplit[15]);
            Log.w("dddGPS-NMEA", "16="+ rawNmeaSplit[16]);
            Log.w("dddGPS-NMEA", "17="+rawNmeaSplit[17]);
            //<2>定位型式 1 = 未定位， 2 = 二維定位， 3 = 三維定位。
            if(!rawNmeaSplit[2].isEmpty())this.ptype=Double.parseDouble(rawNmeaSplit[2]);
            if(!rawNmeaSplit[15].isEmpty())this.pdop=Double.parseDouble(rawNmeaSplit[15]);
            if(!rawNmeaSplit[16].isEmpty())this.hdop=Double.parseDouble(rawNmeaSplit[16]);
            //for Google pixel 2, CAT S61, Nokia 8 GPS chip
        //try{
            if(!rawNmeaSplit[15].isEmpty()&&!rawNmeaSplit[17].isEmpty())this.vdop=Double.parseDouble(rawNmeaSplit[17]);
        //} catch (Exception e) {e.printStackTrace();}
            //for SONY XZs, Samsung S3, HTC m8手機GPS chip
       //     if(!rawNmeaSplit[15].isEmpty()&&!rawNmeaSplit[17].isEmpty()) {
       //         rawNmeaSplit[17] = rawNmeaSplit[17].substring(0, rawNmeaSplit[17].indexOf("*"));
       //         this.vdop=Double.parseDouble(rawNmeaSplit[17]);}
        }else if(rawNmeaSplit[0].equalsIgnoreCase("$GPGSV")){
            Log.d("dddGPS-NMEA", rawNmea);
            Log.d("dddGPS-NMEA", "3="+rawNmeaSplit[3]);
            if(!rawNmeaSplit[3].isEmpty())this.satellite_inview=Double.parseDouble(rawNmeaSplit[3]);

        }else if(rawNmeaSplit[0].equalsIgnoreCase("$GPRMC")){

        }else if(rawNmeaSplit[0].equalsIgnoreCase("$GPVTG")){
            Log.i("dddGPS-NMEA", rawNmea);
            if(!rawNmeaSplit[7].isEmpty())this.speed=Double.parseDouble(rawNmeaSplit[7]);

        }else if(rawNmeaSplit[0].equalsIgnoreCase("$GPGLL")){

        }
        } catch (Exception e) {e.printStackTrace();}

    }


    GpsStatus.NmeaListener nmeaListener = new GpsStatus.NmeaListener() {
        public void onNmeaReceived(long timestamp, String nmea) {
            //check nmea's checksum
            //NmeaParser nmeaparser = new NmeaParser();
            //nmeaparser.parse(nmea);
            nmeaProgress(nmea);
            Log.d("GPS-NMEA", nmea);

        }
    };
}

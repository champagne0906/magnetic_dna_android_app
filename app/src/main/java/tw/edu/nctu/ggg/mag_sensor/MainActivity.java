package tw.edu.nctu.ggg.mag_sensor;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.os.AsyncTask;
import android.content.SharedPreferences;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.net.InetAddress;
import java.net.UnknownHostException;
import org.apache.commons.net.ntp.NTPUDPClient;
import org.apache.commons.net.ntp.TimeInfo;
import java.util.Calendar;

import static android.hardware.Sensor.STRING_TYPE_ACCELEROMETER;
import static android.hardware.Sensor.STRING_TYPE_GYROSCOPE;
import static android.hardware.Sensor.STRING_TYPE_MAGNETIC_FIELD;
import static tw.edu.nctu.ggg.mag_sensor.SensorList.gsensorValues;
import static tw.edu.nctu.ggg.mag_sensor.SensorList.gyroscopeValues;
import static tw.edu.nctu.ggg.mag_sensor.SensorList.infoAccelerometer;
import static tw.edu.nctu.ggg.mag_sensor.SensorList.infoGyroscope;
import static tw.edu.nctu.ggg.mag_sensor.SensorList.infoMagnetic;
import static tw.edu.nctu.ggg.mag_sensor.SensorList.infoPressure;
import static tw.edu.nctu.ggg.mag_sensor.SensorList.magneticValues;
import static tw.edu.nctu.ggg.mag_sensor.SensorList.pressureHeight;
import static tw.edu.nctu.ggg.mag_sensor.SensorList.pressureValue;


public class MainActivity extends AppCompatActivity {
    private final int REQUEST_PERMISSION = 1000;
    private Button butStart,butStop,butInfo,butFigureType;
    private CheckBox chkGGG;
    private TextView tv_debug,runningText,gpsInfo;
    private EditText editText;
    private Handler mHandler;
    private boolean running=false;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
            "yyyy/MM/dd HH:mm:ss");
    private SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat(
            "HH:mm:ss");
    FileWriter fileWriter;

    //20180118 chart test
    private LineChart chart_line;
    private List<Float> datasListX=new ArrayList<Float>(20);
    private List<Float> datasListY=new ArrayList<Float>(20);
    private List<Float> datasListZ=new ArrayList<Float>(20);
    private int currentFigureType=1; //default 1
    private String setGGG="ini";//一個按鈕按下去會記錄ggg 2018/6/18
    int timeCount=0;

    String fileName = System.currentTimeMillis()+".txt";
    File outputFile;
    private SensorManager sensorManager;
    private SensorList sallistener;
    private GPSTracker gps;
    Timer timer = new Timer(true);
    timerTask tsk;
    private String textHistory="",textNew="";
    private PowerManager.WakeLock wlLock;


    protected void onDestroy() {
        // TODO Auto-generated method stub
        Log.d("onDestroy", "onDestroy: ");
        if (wlLock.isHeld())
        wlLock.release();
        timer.cancel();
        sallistener.stopService(sensorManager);
        //unregisterReceiver(ActionFoundReceiver);
        if(fileWriter!=null){
            try {
                fileWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            fileWriter = null;
        }
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        runningText.setTextColor(Color.rgb(255, 0, 0));
        if(running)
            runningText.setTextColor(Color.rgb(0, 255, 0));
        Log.d("main", "onResume: ");
        if (CheckGPSStatus()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (Settings.System.canWrite(this)) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                        initPermission();
                    } else {
                        ShowPermissionSettingMsg();
                    }
            } else {//version under 6.0

            }
        } else {
            ShowGPSClosedMsg();
        }
    }
    private void saveData(String data) {
        // new Date(): Initializes this Date instance to the current time.

        saveDataToFile(data);
    }
    public void saveDataToFile(String data){
        try {
            Log.d("file", ""+data);
            fileWriter.write(data);
            fileWriter.flush();
        } catch (Exception e) {
            Log.d("file error", ""+e.toString());
            e.printStackTrace();
        }}


    /* Checks if external storage is available for read and write */
    private boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    private File getPublicStorageDir(String fileName) {
        // Get the directory for the user's public pictures directory.
        File file = new File(Environment.getExternalStorageDirectory()+"/WIRELAB/", fileName);
        if (!file.mkdirs()) {
            Log.e("getPublicStorageDir", "Directory not created");
        }
        return file;
    }

    private Handler updateHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Bundle receiveBundle = msg.getData();
            String txt = receiveBundle.getString("txt1");
            String list = receiveBundle.getString("list");
            String gpsData = receiveBundle.getString("gps");
            saveData(txt);

            gpsInfo.setText(gpsData);

            chart_line.setData(getLineData());
            chart_line.notifyDataSetChanged();
            chart_line.invalidate();

            //tv_debug.setMovementMethod(ScrollingMovementMethod.getInstance());

            if(textNew.length()>6000) {
                textHistory = textNew;
                textNew="";
            }
            textNew=list+textNew;
            tv_debug.setText(textNew+textHistory);
            //tv_debug.append(list);
            Log.d("0322", "handleMessage: "+tv_debug.getText().length());
            //tv_debug.setText(txt);
 //           editText.append(txt+"\n");
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ShowDialogMsg.mcontext = getApplicationContext();
        //20180118 chart test
        chart_line = (LineChart)findViewById(R.id.chart_line);
        chart_line.setData(getLineData());

        butStart = (Button) findViewById(R.id.butStart);
        butStop = (Button)  findViewById(R.id.butStop);
        butInfo = (Button)  findViewById(R.id.butInfo);
        butFigureType = (Button)  findViewById(R.id.butFigureType);
        chkGGG = (CheckBox) findViewById(R.id.chkGGG);
    //    editText = (EditText) findViewById(R.id.editText);
        tv_debug = (TextView) findViewById(R.id.textView2);
        gpsInfo = (TextView) findViewById(R.id.gpsInfo);
        runningText = (TextView) findViewById(R.id.runningText);
        //sensor
        sallistener = new SensorList();
        sensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        sallistener.startService(sensorManager);
// Wakelock防止休眠影響log
        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        wlLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                "MyWakelockTag");
        wlLock.acquire();

        new GetTimeFromNetwork().execute();


        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            public void run() {
                TextView time = (TextView)findViewById(R.id.textTime);
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.MILLISECOND, (int)difftime);
                Date fixtime = cal.getTime();
                time.setText(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS").format(fixtime));
                handler.postDelayed(this,10);

                TextView textlasttime = (TextView)findViewById(R.id.textLastTime);
                textlasttime.setText(lastTime + " /  修正" + difftime + "ms" );
            }
        };
        handler.postDelayed(runnable,10);
/*
        Timer timer = new Timer();
        TimerTask task;
        final Handler time_handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("dd:MMMM:yyyy HH:mm:ss a");
                String strDate = sdf.format(c.getTime());
                TextView time = (TextView)findViewById(R.id.textTime);
                time.setText(strDate);
                super.handleMessage(msg);
            }
        };

        task = new TimerTask() {
            @Override
            public void run() {
                Message message = new Message();
                time_handler.sendMessage(message);
            }
        };
        timer.schedule(task, 100, 100);*/

/*
//IO  read file
        // read data into (TextFlied)tv_debug from INPUT_FILE
        try {
            BufferedReader bufReader = new BufferedReader(new FileReader(path+"/"+ fileName));
            while (bufReader.ready()) {
                tv_debug.append(bufReader.readLine()+"\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
            tv_debug.append(simpleDateFormat.format(new Date())+" Start Recording... \n");
        }
*/
        tv_debug.append(simpleDateFormat.format(new Date())+" Start Recording... \n");
        File path = getPublicStorageDir("123");
        // Set output file
        if(!isExternalStorageWritable()){
            Log.d("ExternalStorageWritable","isExternalStorageWritable == false");
            return;
        }
        outputFile = new File(path+"/"+ fileName);
        try {
            fileWriter = new FileWriter(outputFile,true);
        } catch (IOException e) {
            e.printStackTrace();
        }


        gps = new GPSTracker(MainActivity.this);

        final String sensorInfo=STRING_TYPE_ACCELEROMETER+"\n"+STRING_TYPE_MAGNETIC_FIELD+"\n"+STRING_TYPE_GYROSCOPE;


        butStart.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (!running) {
                    final View item = LayoutInflater.from(MainActivity.this).inflate(R.layout.start_pop, null);
                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle("time interval (ms)")
                            .setView(item)
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ShowDialogMsg.showDialog("Cancel");
                                }
                            })
                            .setPositiveButton("Start", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    EditText timeInterval = (EditText) item.findViewById(R.id.timeInterval);
                                    String time = timeInterval.getText().toString();
                                    if (!time.equals("")) {
                                        int t = Integer.parseInt(time);
                                        //if(t>50)
                                        running = true;
                                        runningText.setTextColor(Color.rgb(0, 255, 0));
                                        tsk = new timerTask();
                                        timer = new Timer(true);
                                        timer.schedule(tsk, 100, t);
                                    } else {
                                        ShowDialogMsg.showDialog("no time");
                                    }
                                }
                            })
                            .show();
                }
            }
        });
        butStop.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (running){
                    AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this)
                            .setMessage("關閉量測?")
                            .setNegativeButton("否", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            })
                            .setPositiveButton("是", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    running = false;
                                    runningText.setTextColor(Color.rgb(255, 0, 0));
                                    timer.cancel();
                                    //finish();
                                }
                            }).show();
                }
            }
        });
        butInfo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("sensor info");
                builder.setItems(new CharSequence[]
                                {"Accelerometer", "Magnetic", "Gyroscope", "Pressure"},
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // The 'which' argument contains the index position
                                // of the selected item
                                switch (which) {
                                    case 0:
                                        AlertDialog alertDialog1 = new AlertDialog.Builder(MainActivity.this)
                                                .setMessage(infoAccelerometer)
                                                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                    }
                                                }).show();
                                        break;
                                    case 1:
                                        AlertDialog alertDialog2 = new AlertDialog.Builder(MainActivity.this)
                                                .setMessage(infoMagnetic)
                                                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                    }
                                                }).show();
                                        break;
                                    case 2:
                                        AlertDialog alertDialog3 = new AlertDialog.Builder(MainActivity.this)
                                                .setMessage(infoGyroscope)
                                                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                    }
                                                }).show();
                                        break;
                                    case 3:
                                        AlertDialog alertDialog4 = new AlertDialog.Builder(MainActivity.this)
                                                .setMessage(infoPressure)
                                                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                    }
                                                }).show();
                                        break;
                                }
                            }
                        });
                builder.create().show();
/*
                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this)
                            .setTitle("sensor info")
                            .setPositiveButton("Accelerometer", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this)
                                            .setMessage(infoAccelerometer)
                                            .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                }
                                            }).show();
                                }
                            })
                            .setNegativeButton("Magnetic", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this)
                                            .setMessage(infoMagnetic)
                                            .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                }
                                            }).show();
                                }
                            })
                            .setNeutralButton("Gyroscope", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this)
                                            .setMessage(infoGyroscope)
                                            .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                }
                                            }).show();
                                }
                            }).show();*/
                }
        });

        butFigureType.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                final String[] figureTypes = {"null","Accelerometer", "Magnetic", "Barometric"};
                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Set Figure Type")
                        .setMessage("Now is "+figureTypes[currentFigureType])
                        .setPositiveButton("Accelerometer", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if(currentFigureType==1){
                                    AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this)
                                            .setMessage("Now is "+figureTypes[currentFigureType])
                                            .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                }
                                            }).show();
                                }else{
                                    butFigureType.setText("Ⓐ加速");
                                    datasListX.clear();
                                    datasListY.clear();
                                    datasListZ.clear();
                                    currentFigureType=1;
                                }
                            }
                        })
                        .setNegativeButton("Magnetic", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if(currentFigureType==2){
                                    AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this)
                                            .setMessage("Now is "+figureTypes[currentFigureType])
                                            .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                }
                                            }).show();
                                }else{
                                    butFigureType.setText("Ⓜ磁場");
                                    datasListX.clear();
                                    datasListY.clear();
                                    datasListZ.clear();
                                    currentFigureType=2;
                                }
                            }
                        })
                        .setNeutralButton("Barometric", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if(currentFigureType==3){
                                    AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this)
                                            .setMessage("Now is "+figureTypes[currentFigureType])
                                            .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                }
                                            }).show();
                                }else{
                                    butFigureType.setText("B氣壓");
                                    datasListX.clear();
                                    datasListY.clear();
                                    datasListZ.clear();
                                    currentFigureType=3;
                                }
                            }
                        }).show();
                        //目前寫法只能有三個選項故先取消陀螺儀
                        /*.setNeutralButton("Gyroscope", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if(currentFigureType==4){
                                    AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this)
                                            .setMessage("Now is "+figureTypes[currentFigureType])
                                            .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                }
                                            }).show();
                                }else{
                                    butFigureType.setText("Ⓖ陀螺");
                                    datasListX.clear();
                                    datasListY.clear();
                                    datasListZ.clear();
                                    currentFigureType=4;
                                }
                            }
                        }).show();*/
            }
        });
        chkGGG.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if(isChecked)
                {
                    setGGG="ggg";
                }
                else
                {
                    setGGG="xxx";
                }
            }
        });
    }

    private List<Entry> getChartData(List<Float> datasList){
        List<Entry> chartData = new ArrayList<>();
        for(int i=0;i<datasList.size();i++){
            if(datasList.get(i)!=null)
            chartData.add(new Entry(i, datasList.get(i)));
            else
                chartData.add(new Entry(i, 0));
        }
        if(datasList.size()==0)
            chartData.add(new Entry(0, 0));
        return chartData;
    }
    private LineData getLineData(){
        /*if(currentFigureType == 3){
            LineDataSet dataSetA = new LineDataSet(getChartData(datasListX), "氣壓高");
            LineDataSet dataSetB = new LineDataSet(getChartData(datasListY), "氣壓高");
            LineDataSet dataSetC = new LineDataSet(getChartData(datasListZ), "GPS高");
        }*/
        LineDataSet dataSetA = new LineDataSet(getChartData(datasListX), "氣壓高, X");
        LineDataSet dataSetB = new LineDataSet(getChartData(datasListY), "氣壓高, Y");
        LineDataSet dataSetC = new LineDataSet(getChartData(datasListZ), "GPS高, Z");
        List<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSetA.setCircleColor(ColorTemplate.rgb("FF0000"));
        dataSetB.setCircleColor(ColorTemplate.rgb("00FF00"));
        dataSetC.setCircleColor(ColorTemplate.rgb("0000FF"));
        dataSetA.setColor(ColorTemplate.rgb("FF0000"));
        dataSetB.setColor(ColorTemplate.rgb("00FF00"));
        dataSetC.setColor(ColorTemplate.rgb("0000FF"));
        dataSets.add(dataSetA); // add the datasets
        dataSets.add(dataSetB); // add the datasets
        dataSets.add(dataSetC); // add the datasets
        return new LineData(dataSets);
    }


    public class timerTask extends TimerTask {
        public void run() {
            onFire();
        }
        double latitude, longitude, speed, bearing, altitudeG, altitudeN, accuracy, vaccuracy, pdop, hdop, vdop,sat_inview, sat_inuse, updateTime, ptype;
        public void onFire() {
            //設定Wi-Fi hotsopt mode
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.MILLISECOND, (int)difftime);
            String nowTime = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(cal.getTime());

            try {
                gps.getLocation();
                latitude = gps.getLatitude();
                longitude = gps.getLongitude();
                speed = gps.getSpeed();
                bearing = gps.getBearing();
                altitudeG = gps.getAltitudeG();
                altitudeN = gps.getAltitudeN();
                accuracy = gps.getAccuracy();
                vaccuracy = gps.getVaccuracy();
                pdop = gps.getPdop();
                hdop = gps.getHdop();
                vdop = gps.getVdop();
                sat_inview = gps.getSatellite_inview();
                sat_inuse = gps.getSatellite_inuse();
                ptype = gps.getPtype();

                //updateTime = gps.getUpdateTime();
                //if the gps update time > 30s 30000ms
                /*if(System.currentTimeMillis()-updateTime>30000){
                    latitude=1;
                    longitude=2;
                    altitudeG=-1000;
                    altitudeN=-1000;
                }*/
                String data=simpleDateFormat.format(cal.getTime())+ "\t"
                        +latitude + "\t" + longitude+ "\t" + speed+ "\t" +bearing+ "\t" +
                        altitudeG + "\t" + altitudeN + "\t" + accuracy + "\t"+ vaccuracy + "\t" +
                        vdop+ "\t" + hdop+ "\t" + pdop+ "\t" + sat_inview+ "\t" + sat_inuse+ "\t" + ptype + "\t"+
                        pressureHeight+ "\t" +pressureValue+ "\t" +
                        gsensorValues[0]+ "\t" +gsensorValues[1]+ "\t"+gsensorValues[2]+ "\t"+
                        magneticValues[0]+ "\t" +magneticValues[1]+ "\t"+magneticValues[2]+ "\t"+
                        gyroscopeValues[0]+ "\t" +gyroscopeValues[1]+ "\t"+gyroscopeValues[2]+ "\t" +
                        setGGG + "\n";
                String data1=nowTime+ " "
                        +String.format("高%3.2f,%3.2f 氣)%4.2f 磁)%3.2f,%3.2f,%3.2f\n",altitudeG,altitudeN,pressureValue,magneticValues[0],magneticValues[1],magneticValues[2]);

                String gpsData="v="+vdop+"h="+hdop+"p="+pdop+"iv="+sat_inview+"iu="+sat_inuse+"pt="+ptype;
                float tempX=0,tempY=0,tempZ=0;
                if(currentFigureType==1){
                    tempX=gsensorValues[0];tempY=gsensorValues[1];tempZ=gsensorValues[2];
                }else if(currentFigureType==2){
                    tempX=magneticValues[0];tempY=magneticValues[1];tempZ=magneticValues[2];
                }else if(currentFigureType==3){
                    tempX=pressureHeight;tempY=pressureHeight;tempZ=Double.valueOf(altitudeG).floatValue();
                }else if(currentFigureType==4){
                    tempX=gyroscopeValues[0];tempY=gyroscopeValues[1];tempZ=gyroscopeValues[2];
                }
                if(datasListX.size()==20){
                    datasListX.remove(0);datasListY.remove(0);datasListZ.remove(0);
                    datasListX.add(tempX);datasListY.add(tempY);datasListZ.add(tempZ);
                }else{
                    datasListX.add(tempX);datasListY.add(tempY);datasListZ.add(tempZ);
                }
                Message msg=updateHandler.obtainMessage();
                Bundle bundle = new Bundle();
                bundle.putString("txt1", data);
                bundle.putString("list", data1);
                bundle.putString("gps", gpsData);
                msg.setData(bundle);
                updateHandler.sendMessage(msg);
            } catch (Exception e) {
                String data=simpleDateFormat.format(cal.getTime())+ "\t"
                        +latitude + "\t" + longitude+ "\t" + speed+ "\t" +bearing+ "\t" +
                        altitudeG + "\t" + altitudeN + "\t" + accuracy + "\t"+ vaccuracy + "\t" +
                        vdop+ "\t" + hdop+ "\t" + pdop+ "\t" + sat_inview+ "\t" + sat_inuse+ "\t" + ptype + "\t"+
                        pressureHeight+ "\t" +pressureValue+ "\t" +
                        gsensorValues[0]+ "\t" +gsensorValues[1]+ "\t"+gsensorValues[2]+ "\t"+
                        magneticValues[0]+ "\t" +magneticValues[1]+ "\t"+magneticValues[2]+ "\t"+
                        gyroscopeValues[0]+ "\t" +gyroscopeValues[1]+ "\t"+gyroscopeValues[2]+ "\t" +
                        setGGG + "\n";
                String data1=nowTime+ " "
                        +String.format("高%3.2f,%3.2f 氣)%4.2f 磁)%3.2f,%3.2f,%3.2f\n",altitudeG,altitudeN,pressureValue,magneticValues[0],magneticValues[1],magneticValues[2]);

                String gpsData="v="+vdop+"h="+hdop+"p="+pdop+"iv="+sat_inview+"iu="+sat_inuse+"pt="+ptype;
                float tempX=0,tempY=0,tempZ=0;
                if(currentFigureType==1){
                    tempX=gsensorValues[0];tempY=gsensorValues[1];tempZ=gsensorValues[2];
                }else if(currentFigureType==2){
                    tempX=magneticValues[0];tempY=magneticValues[1];tempZ=magneticValues[2];
                }else if(currentFigureType==3){
                    tempX=gyroscopeValues[0];tempY=gyroscopeValues[1];tempZ=gyroscopeValues[2];
                }
                if(datasListX.size()==20){
                    datasListX.remove(0);datasListY.remove(0);datasListZ.remove(0);
                    datasListX.add(tempX);datasListY.add(tempY);datasListZ.add(tempZ);
                }else{
                    datasListX.add(tempX);datasListY.add(tempY);datasListZ.add(tempZ);
                }
                Message msg=updateHandler.obtainMessage();
                Bundle bundle = new Bundle();
                bundle.putString("txt1", data);
                bundle.putString("list", data1);
                bundle.putString("gps", gpsData);
                msg.setData(bundle);
                updateHandler.sendMessage(msg);
            }

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void initPermission() {
        List<String> permissionsNeeded = new ArrayList<>();

        final List<String> permissionsList = new ArrayList<>();
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION)
                || !addPermission(permissionsList, Manifest.permission.ACCESS_COARSE_LOCATION))
            permissionsNeeded.add("LOCATION");
        if (!addPermission(permissionsList, Manifest.permission.READ_EXTERNAL_STORAGE)
                || !addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("STORAGE");
        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                // Need Rationale
                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    message = message + ", " + permissionsNeeded.get(i);
                showMessageOKCancel(message,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                                        REQUEST_PERMISSION);
                            }
                        });
                return;
            }
            requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                    REQUEST_PERMISSION);
            return;
        }
    }
    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(MainActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ShowDialogMsg.showDialog("APP can't start without Permission");
                    }
                })
                .create()
                .show();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean addPermission(List<String> permissionsList, String permission) {
        if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            // Check for Rationale Option
            if (!shouldShowRequestPermissionRationale(permission))
                return false;
        }
        return true;
    }
    /***
     * about android 6.0 permissions
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION: {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
                    break;
                Map<String, Integer> perms = new HashMap<String, Integer>();
                // Initial
                perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.ACCESS_COARSE_LOCATION, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);

                if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                    perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_DENIED);
                if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                    perms.put(Manifest.permission.ACCESS_COARSE_LOCATION, PackageManager.PERMISSION_DENIED);
                if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
                    perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_DENIED);
                if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
                    perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_DENIED);

                // Fill with results
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                // Check for ACCESS_FINE_LOCATION
                if (perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    // All Permissions Granted
                    //StartServices();
                    runningText.setTextColor(Color.rgb(0, 255, 0));
                } else {
                    // Permission Denied
                    Toast.makeText(MainActivity.this, "需要開啟權限", Toast.LENGTH_SHORT)
                            .show();
                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
    private void ShowGPSClosedMsg() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).setTitle("GPS Checker")
                .setMessage("Please Turn Your GPS Service on")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                }).show();
    }
    public void ShowPermissionSettingMsg() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).setTitle("System setting Checker")
                .setMessage("Please Grant System Setting")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(android.provider.Settings.ACTION_MANAGE_WRITE_SETTINGS);
                        intent.setData(Uri.parse("package:" + MainActivity.this.getPackageName()));
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                }).show();
    }
    private boolean CheckGPSStatus() {
        GPSTracker gpsTracker = new GPSTracker(MainActivity.this);
        if(!gpsTracker.isOpenGps()){
            //"Please turn your gps on"
            return false;
        }
        return true;
    }

    public float  difftime = 0;
    public String lastTime = "";
    public class GetTimeFromNetwork extends AsyncTask<String, Void, String> {
        protected void onPreExecute() {
        }

        protected String doInBackground(String... params) {
            SharedPreferences pref = getSharedPreferences("ntp", 0);
            try {
                float sum = 0;
                int check = 10;
                for(int i=0;i<check;i++) {
                    String TIME_SERVER = "tock.stdtime.gov.tw";
                    NTPUDPClient timeClient = new NTPUDPClient();
                    InetAddress inetAddress = InetAddress.getByName(TIME_SERVER);
                    TimeInfo timeInfo = timeClient.getTime(inetAddress);
                    long returnTime = timeInfo.getMessage().getTransmitTimeStamp()
                            .getTime(); //server time
                    Date ntptime = new Date(returnTime);
                    Date nowtime = new Date();
                    float thisDiffTime = ntptime.getTime() - nowtime.getTime();
                    sum += thisDiffTime;
                    Log.i("time", "Time from " + TIME_SERVER + ": " + ntptime);
                }
                pref.edit()
                        .putFloat("diffTime", sum/check)
                        .putString("lastTime", new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS").format(new Date()))
                        .commit();
            } catch (Exception e) {
                Log.e("tutu",e.getMessage());
            }

            difftime = pref.getFloat("diffTime", 0);
            lastTime = pref.getString("lastTime", "NA");

            return null;
        }

        protected void onPostExecute(String result) {
        }
    }

}






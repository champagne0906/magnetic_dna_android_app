package tw.edu.nctu.ggg.mag_sensor;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import java.util.List;


public class SensorList implements SensorEventListener {
    private final String TagName = "SensorList";
    public static String infoAccelerometer,infoMagnetic,infoGyroscope,infoPressure;
    public static float pressureHeight,pressureValue;
    public static float[] gsensorValues =new float[3];
    public static float[] magneticValues =new float[3];
    public static float[] gyroscopeValues=new float[3];
    private float[] r = new float[9];    //rotation matrix
    private float[] tempValues=new float[3];
    public static float[] rValues = new float[3];   //orientation values
    public SensorList() {
        infoAccelerometer=infoMagnetic=infoGyroscope=infoPressure="null";
        pressureHeight=pressureValue=0;
        rValues[0]= rValues[1]= rValues[2]=0;
        gsensorValues[0]= gsensorValues[1]= gsensorValues[2]=0;
        magneticValues[0]= magneticValues[1]= magneticValues[2]=0;
        gyroscopeValues[0]=gyroscopeValues[1]=gyroscopeValues[2]=0;
    }
    //-------------------
    protected void SetSensor(SensorManager sensorManager)
    {
        Sensor mAccelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        if (mAccelerometer == null){
            //No Accelerometer Sensor!
        }else{
            List<Sensor> sensors = sensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER);
            if(sensors.size()>0){
                //Log.d("20180201", "SetSensor: "+sensors.get(0).toString()+"\n"+sensors.get(0).getType()+"\n"+sensors.get(0).getStringType()+"\n");
                infoAccelerometer=sensors.get(0).getName()+"\nVendor:"+sensors.get(0).getVendor()+"\nVersion:"+sensors.get(0).getVersion()+"\nMaximumRange:"+sensors.get(0).getMaximumRange()+"m/s2\nResolution:"+sensors.get(0).getResolution()+"m/s2\nPower:"+sensors.get(0).getPower()+"mA";
            }
            sensorManager.registerListener(this, mAccelerometer, sensorManager.SENSOR_DELAY_FASTEST);
        }
        //Measures the ambient geomagnetic field for all three physical axes (x, y, z) in μT.
        Sensor mMagnetic = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        if (mMagnetic == null){
            //No mMagnetic Sensor!
        }else{
            List<Sensor> sensors = sensorManager.getSensorList(Sensor.TYPE_MAGNETIC_FIELD);
            if(sensors.size()>0){
                infoMagnetic=sensors.get(0).getName()+"\nVendor:"+sensors.get(0).getVendor()+"\nVersion:"+sensors.get(0).getVersion()+"\nMaximumRange:"+sensors.get(0).getMaximumRange()+"μT\nResolution:"+sensors.get(0).getResolution()+"μT\nPower:"+sensors.get(0).getPower()+"mA";
            }
            sensorManager.registerListener(this, mMagnetic,sensorManager.SENSOR_DELAY_FASTEST);
        }
        Sensor mGyroscope = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        if (mGyroscope == null){
            //No mMagnetic Sensor!
        }else{
            List<Sensor> sensors = sensorManager.getSensorList(Sensor.TYPE_GYROSCOPE);
            if(sensors.size()>0){
                infoGyroscope=sensors.get(0).getName()+"\nVendor:"+sensors.get(0).getVendor()+"\nVersion:"+sensors.get(0).getVersion()+"\nMaximumRange:"+sensors.get(0).getMaximumRange()+"°/s\nResolution:"+sensors.get(0).getResolution()+"°/s\nPower:"+sensors.get(0).getPower()+"mA";
            }
            sensorManager.registerListener(this, mGyroscope,sensorManager.SENSOR_DELAY_FASTEST);
        }
        /*Sensor mProximity = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        if (mProximity == null){
            //No Proximity Sensor!
        }else{
            sensorManager.registerListener(this, mProximity,sensorManager.SENSOR_DELAY_NORMAL);
        }*/
        //Measures the ambient light level (illumination) in lx.
        /*Sensor mLight = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        if (mLight == null){
            //No mLight Sensor!
        }else{
            sensorManager.registerListener(this, mLight,sensorManager.SENSOR_DELAY_NORMAL);
        }*/
        //Measures the ambient air pressure in hPa or mbar.
        Sensor mPressure = sensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE);
        if (mPressure == null){
            //No mPressure Sensor!
        }else{
            List<Sensor> sensors = sensorManager.getSensorList(Sensor.TYPE_PRESSURE);
            if(sensors.size()>0){
                infoPressure=sensors.get(0).getName()+"\nVendor:"+sensors.get(0).getVendor()+"\nVersion:"+sensors.get(0).getVersion()+"\nMaximumRange:"+sensors.get(0).getMaximumRange()+"°/s\nResolution:"+sensors.get(0).getResolution()+"°/s\nPower:"+sensors.get(0).getPower()+"mA";
            }
            sensorManager.registerListener(this, mPressure,sensorManager.SENSOR_DELAY_FASTEST);
        }
    }

    public void startService(SensorManager sensorManager) {
        SetSensor(sensorManager);
    }
    public void stopService(SensorManager sensorManager) {
        sensorManager.unregisterListener(this);
    }
    //Accelerometer, Light, Proximity, Barometer, Magnetometer

    @Override
    public void onSensorChanged(final SensorEvent event) {
        // TODO Auto-generated method stub
        switch (event.sensor.getType()) {
            case Sensor.TYPE_ACCELEROMETER:
                gsensorValues[0]=event.values[0];
                gsensorValues[1]=event.values[1];
                gsensorValues[2]=event.values[2];
                SensorManager.getRotationMatrix(r, null, gsensorValues, magneticValues);
                SensorManager.getOrientation(r, tempValues);
                rValues[0]=(float) Math.toDegrees(tempValues[0]);
                rValues[1]=(float) Math.toDegrees(tempValues[1]);
                rValues[2]=(float) Math.toDegrees(tempValues[2]);
                break;
            case Sensor.TYPE_MAGNETIC_FIELD://Measures the ambient geomagnetic field for all three physical axes (x, y, z) in μT
                magneticValues[0]=event.values[0];
                magneticValues[1]=event.values[1];
                magneticValues[2]=event.values[2];
                SensorManager.getRotationMatrix(r, null, gsensorValues, magneticValues);
                SensorManager.getOrientation(r, tempValues);
                rValues[0]=(float) Math.toDegrees(tempValues[0]);
                rValues[1]=(float) Math.toDegrees(tempValues[1]);
                rValues[2]=(float) Math.toDegrees(tempValues[2]);
                break;
            case Sensor.TYPE_GYROSCOPE://Measures the ambient geomagnetic field for all three physical axes (x, y, z) in μT
                gyroscopeValues[0]=event.values[0];
                gyroscopeValues[1]=event.values[1];
                gyroscopeValues[2]=event.values[2];
                break;
            case Sensor.TYPE_PRESSURE://hPa
                pressureValue=event.values[0];
                pressureHeight =SensorManager.getAltitude(SensorManager.PRESSURE_STANDARD_ATMOSPHERE,event.values[0]);
                break;
        }

    }
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // TODO Auto-generated method stub
    }
}
